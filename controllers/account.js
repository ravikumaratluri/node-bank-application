/** 
*  Account controller
*  Handles requests related to Account (see routes)
*
* @author Varun Goud Pulipalpula <s534848@nwmissouri.edu>
*
*/
const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const Model = require('../models/account.js')
const notfoundstring = 'account not found'

// RESPOND WITH JSON DATA  --------------------------------------------

// GET all JSON
api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.accounts.query
  console.log(`Number of account Records: ${data.length}`)
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  console.log(`Number of account Records: ${data.length}`)
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})

// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)
api.get('/', (req, res) => {
  res.render('account/home.ejs')
})

// GET index
api.get('/index', (req, res) => {
  res.render('account/index.ejs')
})


// GET create
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(item)
  LOG.debug(JSON.stringify(item))
  res.render('account/create',
    {
      title: 'Create Account',
      layout: 'layout.ejs',
      account: item
    })
})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  console.log(`Number of account Records: ${data.length}`)
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('account/delete.ejs',
    {
      title: 'Delete account',
      layout: 'layout.ejs',
      account: item
    })
})

// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  console.log(`Number of account Records: ${data.length}`)
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('account/details.ejs',
    {
      title: 'Confirm account Delete',
      layout: 'layout.ejs',
      account: item
    })
})

// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('account/edit.ejs',
    {
      title: 'Edit account',
      layout: 'layout.ejs',
      account: item
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.accounts.query
  const item = new Model()
  console.log(`Number of account Records: ${data.length}`)
  LOG.info(`NEW body ${JSON.stringify(req.body)}`)
  LOG.info(`NEW ID ${req.body._id}`)
  item._id = parseInt(req.body._id)
  item.accountnumber = parseInt(req.body.accountnumber)
  item.accountName = req.body.accountName
  item.userId = parseInt(req.body.userId)
  item.transactionId = parseInt(req.body.transactionId)
  data.push(item)
  LOG.info(item)
  LOG.info(`SAVING NEW account ${JSON.stringify(item)}`)
  return res.redirect('/account')
})

// POST update with id
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.accounts.query
  console.log(`Number of account Records: ${data.length}`)
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item.accountnumber = parseInt(req.body.accountnumber)
  item.accountName = req.body.accountName
  item.userId = parseInt(req.body.userId)
  item.transactionId = parseInt(req.body.transactionId)
  LOG.info(`SAVING UPDATED account ${JSON.stringify(item)}`)
  return res.redirect('/account')
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.accounts.query
  console.log(`Number of account Records: ${data.length}`)
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  if (item.isActive) {
    item.isActive = false
    console.log(`Deacctivated item ${JSON.stringify(item)}`)
  } else {
    const item = remove(data, { _id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  }
  return res.redirect('/account')
})


module.exports = api
